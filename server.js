const express = require('express');
const app = express();
const mongoose = require('mongoose');
const morgan = require('morgan');
const { PORT, mongoUri } = require('./config');
const cors = require('cors');
const bodyParser = require('body-parser');
const userRoutes = require('./routes/api/users');
const path = require('path');

app.use(cors());
app.use(bodyParser.json());
app.use(morgan('dev'));

mongoose
  .connect(mongoUri, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(() => console.log('MongoDB database connected...'))
  .catch((err) => console.log(err));

app.use('/api/users', userRoutes);

if (process.env.NODE_ENV === 'production') {
  app.use(express.static('client/build'));
  app.use('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'client', 'build', 'index.html'));
  });
}

app.listen(PORT, () => console.log(`App listening at http://localhost:${PORT}`));