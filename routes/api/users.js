const { Router } = require('express');
const User = require('../../models/User');

const router = Router();

router.get('/', async (req, res) => {
  try {
    const users = await User.find();

    if (!users) throw new Error('No users found');

    res.status(200).json(users);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

router.post('/', async (req, res) => {
  const newUser = new User(req.body);

  try {
    const user = await newUser.save();

    if (!user) throw new Error('Something went wrong saving the user');

    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

router.put('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const newUser = req.body;
    const user = await User.findByIdAndUpdate(id, newUser);

    if (!user) throw new Error('Something went wrong updating the user');
    
    newUser._id = id;

    res.status(200).json(newUser);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

router.delete('/:id', async (req, res) => {
  const { id } = req.params;

  try {
    const user = await User.findByIdAndDelete(id, req.body);

    if (!user) throw new Error('Something went wrong deleting the user');

    res.status(200).json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
});

module.exports = router;