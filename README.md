# Getting Started with Users App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Assumptions

* Node v14.15.4 is installed on your machine.

* Used functional components because they are easier to read, debug, and test.<br/>
They offer performance benefits, decreased coupling, and greater reusability.

* This app is designed with service layer for easier and structured way to swap out data from source.
## Overview

* User can create, edit, delete data from the database.

* Users are being displayed using a table element.

* Used modal for updating user.

* This app is mobile responsive.

The source code is available in GitLab and can be accessed via the following url:

* https://gitlab.com/michaelpaulquimson/mern-stack-app
## Available Scripts

In the project directory, you can run:

* ### `npm run build`

	Installs npm packages of client and server.

* ### `npm run dev`

	Runs the app and server in the development mode.<br/>
	Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

	The page will reload if you make edits.<br/>
	You will also see any lint errors in the console.