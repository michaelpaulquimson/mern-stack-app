import User from '../models/UserModel';
import axios from 'axios';

const UsersService = {
  getUsers: async function() {
    try {
      const response = await axios.get('api/users/');
      const payload = response.data;
      const users = payload ? payload.map((user) => new User(user)) : [];
      return users;
    } catch (error) {
      console.log(error);
    }
  },
  createUser: async function(user) {
    try {
      const response = await axios.post('api/users/', {
        firstname: user.firstname,
        lastname: user.lastname,
        deliveryAddress: user.deliveryAddress,
        billingAddress: user.billingAddress,
      });
      const createdUser = response.data;
      return createdUser;
    } catch (error) {
      console.log(error);
    }
  },
  deleteUser: async function(id) {
    try {
      const response = await axios.delete('api/users/' + id);
      const deletedUser = response.data;
      return deletedUser;
    } catch (error) {
      console.log(error);
    }
  },updateUser: async function(user) {
    try {
      const response = await axios.put('api/users/' + user.id, {
        firstname: user.firstname,
        lastname: user.lastname,
        deliveryAddress: user.deliveryAddress,
        billingAddress: user.billingAddress,
      });
      const updatedUser = response.data;
      return updatedUser;
    } catch (error) {
      console.log(error);
    }
  }
}

export default UsersService;