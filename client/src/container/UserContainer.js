import React, { useEffect, useState } from 'react';
import UsersService from '../services/UsersService';
import UserList from '../components/UserList';
import CreateUser from '../components/CreateUser';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 8px;
  padding: 8px;
`;

const Header = styled.h1`
  font-size: 30px;
  font-weight: bold;
  margin: 8px;
`;

const UserContainer = () => {
  const [users, setUser] = useState([]);

  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    const users = await UsersService.getUsers();
    if (users) setUser(users);
  }

  async function handleDeleteUser(id) {
    const deletedUser = await UsersService.deleteUser(id);
    setUser(users.filter(user => user.id !== deletedUser._id));
  }

  async function handleCreateUser(newUser) {
    const userToCreate = {
			...newUser
		};
    const createdUser = await UsersService.createUser(newUser);
    userToCreate.id = createdUser._id;
    if (createdUser) {
      setUser([
        ...users,
        userToCreate
      ])
    };
  }

  async function handleUpdateUser(updateUser) {
    const updatedUser = await UsersService.updateUser(updateUser);
    setUser(users.map(user => {
			if (user.id === updatedUser._id) {
				return updateUser;
			}
			return user;
		}));
  }

  return (
    <Container>
      <Header>USER LIST</Header>
      <CreateUser handleCreateUser={(newUser) => handleCreateUser(newUser)} />
      <br />
      <br />
      <UserList Users={users} handleUpdateUser={(updateUser) => handleUpdateUser(updateUser)} handleDeleteUser={(id) => handleDeleteUser(id)} />
    </Container>
  );
};

export default UserContainer;