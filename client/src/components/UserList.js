import React from 'react';
import UpdateUser from '../components/UpdateUser';
import { Table, Thead, Tbody, Tr, Th, Td } from 'react-super-responsive-table';
import 'react-super-responsive-table/dist/SuperResponsiveTableStyle.css';
import styled from 'styled-components';

const Container = styled.div`
  max-width: 1440px;
	max-height: 1024px;
	margin: auto;
  width: 100%;

  table {
		font-family: Arial, Helvetica, sans-serif;
		border-collapse: collapse;
		width: 100%;
		margin: 25px 0;
	}

	table td, table th {
		border: 1px solid #ddd;
		padding: 8px;
		text-align: center;
	}

	table tr:nth-child(even){background-color: #f2f2f2;}

	table tr:hover {background-color: #ddd;}

	table th {
		white-space: pre;
		padding-top: 12px;
		padding-bottom: 12px;
		background-color: #4CAF50;
		color: white;
		font-weight: bold;
	}
`;

const Header = styled.h1`
  font-size: 20px;
  font-weight: bold;
  margin: 8px;
  text-align: center;
`;

const StyleButton = styled.span`
  color: blue;
  cursor: pointer;
`;

const UserList = ({Users, handleUpdateUser, handleDeleteUser}) => {
  return (
    <Container>
      <Header>USER LIST</Header>
      <Table>
        <Thead>
          <Tr>
            <Th>FIRSTNAME</Th>
            <Th>LASTNAME</Th>
            <Th>DELIVERY</Th>
            <Th>BILLING</Th>
            <Th colSpan='2'>ACTION</Th>
          </Tr>
        </Thead>
        <Tbody>
          {Users.map(user => {
            return (
              <Tr key={user.id}>
                <Td>{user.firstname}</Td>
                <Td>{user.lastname}</Td>
                <Td>{user.deliveryAddress}</Td>
                <Td>{user.billingAddress}</Td>
                <Td><StyleButton onClick={() => handleDeleteUser(user.id)}>Delete</StyleButton></Td>
                <Td><UpdateUser User={user} handleUpdateUser={(updateUser) => handleUpdateUser(updateUser)} /></Td>
              </Tr>
            );
          })}
        </Tbody>
      </Table>
    </Container>
  );
};

export default UserList;