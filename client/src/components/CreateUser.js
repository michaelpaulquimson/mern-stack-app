import React, { useState } from 'react';
import styled from 'styled-components';

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.h1`
  font-size: 20px;
  font-weight: bold;
  margin: 8px;
`;

const Input = styled.input`
  border: 1px solid black;
  margin: 8px;
  width: 250px;
  border-radius: 5px;
  padding: 8px;
`;

const Label = styled.label`
  padding: 8px;
`;

const Button = styled.button`
  border: 1px solid black;
  margin: 20px 8px;
  width: 250px;
  border-radius: 5px;
  padding: 8px;
  text-align: center;
  background-color: lightgrey;
  cursor: pointer;
  ${(props) => props.hasError && `
		opacity: .2;
	`};
`;

const ErrorMsg = styled.p`
  color: red;
  padding: 8px;
`;

const CreateUser = ({handleCreateUser}) => {
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [deliveryAddress, setDeliveryAddress] = useState('');
  const [billingAddress, setBillingAddress] = useState('');

  function handleClick() {
    hasError();
    const newUser = {
      firstname,
      lastname,
      deliveryAddress,
      billingAddress
    };
    if (!hasError()) {
      handleCreateUser(newUser);
      setFirstname('');
      setLastname('');
      setDeliveryAddress('');
      setBillingAddress('');
    }
  }

  function hasError() {
    if (firstname.length === 0) return true;
    if (lastname.length === 0) return true;
    if (deliveryAddress.length === 0) return true;
    if (billingAddress.length === 0) return true;
    return false;
  }

  return (
    <Container>
      <Header>CREATE USER</Header>
      <Label>Firstname</Label>
      <Input value={firstname} onChange={(e) => setFirstname(e.target.value)} />
      {firstname.length === 0 && <ErrorMsg>*required</ErrorMsg>}
      <Label>Lastname</Label>
      <Input value={lastname} onChange={(e) => setLastname(e.target.value)} />
      {lastname.length === 0 && <ErrorMsg>*required</ErrorMsg>}
      <Label>DeliveryAddress</Label>
      <Input value={deliveryAddress} onChange={(e) => setDeliveryAddress(e.target.value)} />
      {deliveryAddress.length === 0 && <ErrorMsg>*required</ErrorMsg>}
      <Label>BillingAddress</Label>
      <Input value={billingAddress} onChange={(e) => setBillingAddress(e.target.value)} />
      {billingAddress.length === 0 && <ErrorMsg>*required</ErrorMsg>}
      <Button hasError={hasError()} onClick={() => handleClick()}>SUBMIT</Button>
    </Container>
  )
};

export default CreateUser;