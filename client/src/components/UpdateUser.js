import React, { useState } from 'react';
import styled from 'styled-components';
import Modal from 'react-modal';

const customStyles = {
	content : {
		top                   : '50%',
		left                  : '50%',
		right                 : 'auto',
		bottom                : 'auto',
		marginRight           : '-50%',
		transform             : 'translate(-50%, -50%)'
	}
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

const Header = styled.h1`
  font-size: 20px;
  font-weight: bold;
  margin: 8px;
`;

const Input = styled.input`
  border: 1px solid black;
  margin: 8px;
  width: 250px;
  border-radius: 5px;
  padding: 8px;
`;

const Label = styled.label`
  padding: 8px;
`;

const Button = styled.button`
  border: 1px solid black;
  margin: 20px 8px;
  width: 250px;
  border-radius: 5px;
  padding: 8px;
  text-align: center;
  background-color: lightgrey;
  cursor: pointer;
  ${(props) => props.hasError && `
		opacity: .2;
	`};
`;

const ErrorMsg = styled.p`
  color: red;
  padding: 8px;
`;

const StyleButton = styled.div`
  color: blue;
  cursor: pointer;
`;

const UpdateUser = ({User, handleUpdateUser}) => {
  const [id] = useState(User.id);
  const [firstname, setFirstname] = useState(User.firstname);
  const [lastname, setLastname] = useState(User.lastname);
  const [deliveryAddress, setDeliveryAddress] = useState(User.deliveryAddress);
  const [billingAddress, setBillingAddress] = useState(User.billingAddress);
  const [modalIsOpen,setIsOpen] = useState(false);

  function handleClick() {
    const updateUser = {
      id: id,
      firstname: firstname,
      lastname: lastname,
      deliveryAddress: deliveryAddress,
      billingAddress: billingAddress
    };
    if (!hasError()) {
      handleUpdateUser(updateUser);
      closeModal();
    }
  }

  function openModal() {
		setIsOpen(true);
	}

  function closeModal(){
		setIsOpen(false);
	}

  function hasError() {
    if (firstname.length === 0) return true;
    if (lastname.length === 0) return true;
    if (deliveryAddress.length === 0) return true;
    if (billingAddress.length === 0) return true;
    return false;
  }

  return (
    <>
    <StyleButton onClick={openModal}>Edit</StyleButton>
    {modalIsOpen && <Modal
				isOpen={modalIsOpen}
				onRequestClose={closeModal}
				style={customStyles}
				contentLabel="Modal"
				ariaHideApp={false}
			>
        <Container>
          <Header>UPDATE USER</Header>
          <Label>Firstname</Label>
          <Input value={firstname} onChange={(e) => setFirstname(e.target.value)} />
          {firstname.length === 0 && <ErrorMsg>*required</ErrorMsg>}
          <Label>Lastname</Label>
          <Input value={lastname} onChange={(e) => setLastname(e.target.value)} />
          {lastname.length === 0 && <ErrorMsg>*required</ErrorMsg>}
          <Label>DeliveryAddress</Label>
          <Input value={deliveryAddress} onChange={(e) => setDeliveryAddress(e.target.value)} />
          {deliveryAddress.length === 0 && <ErrorMsg>*required</ErrorMsg>}
          <Label>BillingAddress</Label>
          <Input value={billingAddress} onChange={(e) => setBillingAddress(e.target.value)} />
          {billingAddress.length === 0 && <ErrorMsg>*required</ErrorMsg>}
          <Button hasError={hasError()} onClick={() => handleClick()}>SUBMIT</Button>
          <Button onClick={() => closeModal()}>CANCEL</Button>
        </Container>
      </Modal>}
    </>
  )
};

export default UpdateUser;