import './App.css';
import UserContainer from './container/UserContainer';

function App() {
  return (
    <div>
      <UserContainer />
    </div>
  );
}

export default App;
