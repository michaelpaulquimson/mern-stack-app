class User {
	constructor({ _id, firstname, lastname, deliveryAddress, billingAddress }) {
		this.id = _id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.deliveryAddress = deliveryAddress;
		this.billingAddress = billingAddress;
	}
}

export default User;